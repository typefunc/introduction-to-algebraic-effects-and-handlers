# Lecture 3: Equational reasoning

In [Lecture 2](lecture-2.md) we focused on designing a programming language,
ignoring the fact that algebraic theories consist not only of signatures but
also *equations*. In this lecture we shall take a closer look at how to use
equations to prove properties of programs.

