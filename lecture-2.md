# Lecture 2: Designing a programming language

In the second lecture we shall focus on properly defining a core programming
language with algebraic effects and handlers, based on the algebraic theories
from [Lecture 1](lecture-1.md).

## Outline

* Syntax of core language
* Operational semantics
* An effect system
* Progress & preservation
* Implementation

## Reading material

There are many possible ways and choices of designing a programming language
around algebraic operations and handlers, but we shall mostly rely on [An effect
system for algebraic effects and handlers](https://arxiv.org/abs/1306.6316).

## Problems

Still to be thought of.

