# Lecture 4: Programming with algebraic effects and handlers

In the last lecture we shall explore how algebraic operations and handlers can
be used in programming.

## Outline

* Basic examples:
  * exceptions
  * state
  * I/O
* Non-determinism and search:
  * ambivalent choice
  * search
  * selection operators
* Transactions
* Probabilistic programming
* Cooperative multi-threading

## Reading material

We shall draw on examples from [An introduction to algebraic effects and
handlers](http://www.eff-lang.org/handlers-tutorial.pdf) and [Programming with
algebraic effects and handlers](https://arxiv.org/abs/1203.1539). People 
